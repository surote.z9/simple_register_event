from flask import Flask,render_template,request
import csv
import os

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('hello.html')

@app.route('/handle_data', methods=['POST'])
def handle_data():
    employee_register = request.form['employee-name']
    print(employee_register)
    write_data(employee_register)
    return render_template('hello.html')

def write_data(name):
    with open(os.path.dirname(os.path.realpath(__file__))+"\\"+"regist_name.csv","a" ,encoding='utf-8',newline='') as out:
        out.write(name)
        out.write('\n')



@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % username

if __name__ == '__main__':
    app.run()
